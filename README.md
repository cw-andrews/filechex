filechex
========

A utility package designed to compare files to determine whether source files should be processed.

Usage
-----

Installation
------------

Requirements
------------
Python 3.6+

Compatibility
-------------

Licence
-------

Authors
-------

`filechex` was written by `cw-andrews <cwandrews.oh@gmail.com>`_.
